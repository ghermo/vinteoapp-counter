# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary - this repository is the android code for the vinteo people app counter
* Version - prototype version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
1. clone this repo
2. setup opencv library v2.4.10 - http://sourceforge.net/projects/opencvlibrary/files/opencv-android/2.4.10/
3. install and setup NDK - https://developer.android.com/ndk/downloads/index.html

* Configuration
setup local.properties - add ndk.dir
* Dependencies
1. NDK
2. OPENCV 
* Database configuration
* How to run tests
* Deployment instructions
in a terminal / cmd prompt - cd app/src/main then run # ndk-build

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* REPO ADMIN - greg.hermo@gmail.com
* Other community or team contact