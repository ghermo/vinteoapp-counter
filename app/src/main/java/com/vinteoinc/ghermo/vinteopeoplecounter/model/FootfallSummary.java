package com.vinteoinc.ghermo.vinteopeoplecounter.model;

/**
 * Created by darla on 6/18/15.
 */
public class FootfallSummary {

    private String date;
    private String startTime;
    private String endTime;
    private int countAB = 0;
    private int countBA = 0;
    private String deviceid;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {

        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getCountAB() {
        return countAB;
    }

    public void setCountAB(int countAB) {
        this.countAB = countAB;
    }

    public int getCountBA() {
        return countBA;
    }

    public void setCountBA(int countBA) {
        this.countBA = countBA;
    }

    public void addCountAB() {
        countAB++;
    }

    public void addCountBA() {
        countBA++;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }
}
