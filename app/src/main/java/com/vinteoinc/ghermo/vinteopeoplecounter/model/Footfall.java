package com.vinteoinc.ghermo.vinteopeoplecounter.model;

/**
 * Created by darla on 5/31/15.
 */
public class Footfall {

    public enum STATE {
        A, B
    }

    private String t;

    private String s;

    public Footfall(String ts, String st){
        this.t = ts;
        this.s = st;
    }

    @Override
    public String toString() {
        return t + "-" + s;
    }

    public String getState(){
        return s;
    }
}