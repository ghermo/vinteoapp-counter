package com.vinteoinc.ghermo.vinteopeoplecounter.bgs;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.BackgroundSubtractorMOG2;

/**
 * Created by grg021 on 5/23/15.
 */
public class BgSubtractor {

    public Mat getForeGroundMask(BackgroundSubtractorMOG2 subtractorMOG2, CameraBridgeViewBase.CvCameraViewFrame inputFrame,
                                 Mat mGrayNew,  Mat mRgbNew, Mat mFGMaskNew){
        mGrayNew = inputFrame.gray(); //I chose the gray frame because it should require less resources to process

        Imgproc.cvtColor(mGrayNew, mRgbNew, Imgproc.COLOR_GRAY2RGB);


        subtractorMOG2.apply(mRgbNew, mFGMaskNew, 0.1);
        Imgproc.morphologyEx(mFGMaskNew, mFGMaskNew, Imgproc.MORPH_CLOSE, new Mat());
        return mFGMaskNew;
    }

}
