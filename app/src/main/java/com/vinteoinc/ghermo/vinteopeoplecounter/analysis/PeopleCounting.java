package com.vinteoinc.ghermo.vinteopeoplecounter.analysis;

import com.google.gson.Gson;
import com.vinteoinc.ghermo.vinteopeoplecounter.model.Footfall;
import com.vinteoinc.ghermo.vinteopeoplecounter.model.FootfallListener;
import com.vinteoinc.ghermo.vinteopeoplecounter.model.FootfallSummary;

import org.opencv.core.Mat;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by grg021 on 5/13/15.
 */
public class PeopleCounting {

    public static final int INTERVAL_SECS = 15;
    private long mNativeObj = 0;

    private int footfallIn = 0;
    private int footfallOut = 0;
    private String macAddr;
    private String gatewayNumber;

    //    SimpleDateFormat dateFormatter = new SimpleDateFormat("MM-dd-yyyy");
    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");

    private List<Footfall> footfallList = new ArrayList<Footfall>();
    private FootfallSummary summary;
    private Long startTime;

    public PeopleCounting() {
        mNativeObj = nativeCreateObject();
    }

    public void release() {
        nativeDestroyObject(mNativeObj);
        mNativeObj = 0;
    }

    public long setInput(Mat input) {
        return nativeSetInput(mNativeObj, input.getNativeObjAddr());
    }

    public long setTracks(long tracks) {
        return nativeSetTracks(mNativeObj, tracks);
    }

    public void process() {
        if (startTime == null) {
            initializeAll();
        }

        nativeProcess(mNativeObj);

        pushFootfallSummaryIfApplicable();
    }

    private static native long nativeCreateObject();

    private native void nativeProcess(long thiz);

    private static native void nativeDestroyObject(long thiz);

    private static native long nativeSetTracks(long thiz, long tracks);

    private static native long nativeSetInput(long thiz, long input);


    public void incrementFootfallAB() {
//        footfallIn++;
        addFootfall(Footfall.STATE.B);
    }

    public void incrementFootfallBA() {
//        footfallOut++;
        addFootfall(Footfall.STATE.A);
    }

    private void initializeAll() {
        startTime = System.currentTimeMillis();

        summary = new FootfallSummary();
        Date currentDate = new Date();
        summary.setDate(dateFormatter.format(currentDate));
        summary.setStartTime(timeFormatter.format(currentDate));
        summary.setDeviceid(this.macAddr);

        footfallList.clear();
    }

    private void addFootfall(Footfall.STATE state) {
        footfallList.add(new Footfall(timeFormatter.format(new Date()), state.toString()));
    }

    private void pushFootfallSummaryIfApplicable() {
        Long currentTime = System.currentTimeMillis();
        boolean isIntervalTimeLapsed = currentTime > (startTime + (INTERVAL_SECS * 1000));

        if (isIntervalTimeLapsed && footfallList.size() != 0) {
            summary.setEndTime(timeFormatter.format(new Date()));
            new PushFootfallDataTask().execute(getFootFallSummaryJSON(), this.gatewayNumber);
            initializeAll();
        }
    }


    private String getFootFallJSON() {
        Gson gson = new Gson();
        return gson.toJson(footfallList);
    }

    private String getFootFallSummaryJSON() {
        Gson gson = new Gson();
        for (Footfall footfall : footfallList) {
            if (Footfall.STATE.A.toString().equals(footfall.getState())) {
                summary.addCountBA();
            } else {
                summary.addCountAB();
            }
        }

        return gson.toJson(summary);
    }


    public String getMacAddr() {
        return macAddr;
    }

    public void setMacAddr(String macAddr) {
        this.macAddr = macAddr;
    }

    public String getGatewayNumber() {
        return gatewayNumber;
    }

    public void setGatewayNumber(String gatewayNumber) {
        this.gatewayNumber = gatewayNumber;
    }
}
