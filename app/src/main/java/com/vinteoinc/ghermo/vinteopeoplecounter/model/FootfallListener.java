package com.vinteoinc.ghermo.vinteopeoplecounter.model;

/**
 * Created by darla on 5/31/15.
 */
public interface FootfallListener {

    void checkAndPushFootfallData();
}
