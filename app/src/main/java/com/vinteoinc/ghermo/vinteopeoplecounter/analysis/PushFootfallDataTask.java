package com.vinteoinc.ghermo.vinteopeoplecounter.analysis;

import android.os.AsyncTask;
import android.telephony.SmsManager;
import android.widget.Toast;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Created by darla on 5/31/15.
 */
public class PushFootfallDataTask extends AsyncTask<String, Void, String> {

    private String toPhoneNumber;
    private SmsManager smsManager;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        smsManager = SmsManager.getDefault();
    }

    @Override
    protected String doInBackground(String... params) {
        this.toPhoneNumber = params[1];
        return params[0];
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        smsManager.sendTextMessage(toPhoneNumber, null, s, null, null);
    }

}
