package com.vinteoinc.ghermo.vinteopeoplecounter;

import android.app.Activity;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.vinteoinc.ghermo.vinteopeoplecounter.analysis.BlobTracking;
import com.vinteoinc.ghermo.vinteopeoplecounter.analysis.PeopleCounting;
import com.vinteoinc.ghermo.vinteopeoplecounter.bgs.BgSubtractor;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.video.BackgroundSubtractorMOG2;


public class MainActivity extends Activity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private static final String TAG = "PeopleCounter::Activity";
    private BackgroundSubtractorMOG2 sub;
    private BgSubtractor subtractor;
    private CameraBridgeViewBase mOpenCvCameraView;

    private Mat mRgba;
    private Mat mGray;
    private Mat mFGMaskNew;
    private Mat blurred;
    private Mat rgba;

    private BlobTracking blobTracking;
    private PeopleCounting peopleCounting;
    private SmsManager smsManager;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
//                    Log.i(TAG, "OpenCV loaded successfully");

                    System.loadLibrary("blobtracking");
                    System.loadLibrary("peoplecounting");
                    sub = new BackgroundSubtractorMOG2(300, 32.0f, false);
                    subtractor = new BgSubtractor();

                    mOpenCvCameraView.enableView();


                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.fd_activity_surface_view);
        mOpenCvCameraView.setCvCameraViewListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
//            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_10, this, mLoaderCallback);
        } else {
//            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        mGray = new Mat();
        mRgba = new Mat();
        mFGMaskNew = new Mat();

        blobTracking = new BlobTracking();
        blurred = new Mat();
        peopleCounting = new PeopleCounting();
        rgba = new Mat();
        peopleCounting.setMacAddr(Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID));
        peopleCounting.setGatewayNumber(this.getString(R.string.gatewayNumber));
    }

    public void onCameraViewStopped() {
        mGray.release();
        mRgba.release();
        mFGMaskNew.release();
        blurred.release();
        blobTracking.release();
        peopleCounting.release();
        rgba.release();
    }

    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {

        rgba = inputFrame.rgba();
        blobTracking.process(rgba, subtractor.getForeGroundMask(sub, inputFrame, mGray, mRgba, mFGMaskNew), blurred);
        peopleCounting.setInput(rgba);
        peopleCounting.setTracks(blobTracking.getTracks());
        peopleCounting.process();

        return rgba;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
