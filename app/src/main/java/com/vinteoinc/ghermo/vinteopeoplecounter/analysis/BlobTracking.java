package com.vinteoinc.ghermo.vinteopeoplecounter.analysis;

import org.opencv.core.Mat;

/**
 * Created by grg021 on 5/23/15.
 */
public class BlobTracking {

    private long mNativeObj = 0;

    public BlobTracking() {
        mNativeObj = nativeCreateObject();
    }

    public void release() {
        nativeDestroyObject(mNativeObj);
        mNativeObj = 0;
    }

    public long getTracks() {
        return nativeGetTracks(mNativeObj);
    }

    public void process(Mat img_input, Mat img_mask, Mat img_output) {
        nativeProcess(mNativeObj, img_input.getNativeObjAddr(), img_mask.getNativeObjAddr(), img_output.getNativeObjAddr());
    }

    private static native long nativeCreateObject();
    private static native void nativeProcess(long thiz, long img_input, long img_mask, long img_output);
    private static native void nativeDestroyObject(long thiz);
    private static native long nativeGetTracks(long thiz);

}