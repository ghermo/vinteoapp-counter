#pragma once

#include <iostream>
#include <string>
#include <cv.h>
#include <highgui.h>

#include "../blobtracking/package_tracking/cvblob/cvblob.h"

enum VehiclePosition
{
  VP_NONE = 0,
  VP_A  = 1,
  VP_B  = 2
};

class PeopleCounting
{
private:
enum LaneOrientation
{
  LO_NONE       = 0,
  LO_HORIZONTAL = 1,
  LO_VERTICAL   = 2
};


  bool firstTime;
  bool showOutput;
  int key;
  cv::Mat img_input;
  cvb::CvTracks tracks;
  std::map<cvb::CvID, std::vector<CvPoint2D64f> > points;
  LaneOrientation LaneOrientation;
  std::map<cvb::CvID, VehiclePosition> positions;
  long countAB;
  long countBA;
  int img_w;
  int img_h;
  int showAB;

public:
  PeopleCounting();
  ~PeopleCounting();

  void setInput(const cv::Mat &i);
  void setTracks(const cvb::CvTracks &t);
  void process(JNIEnv *jenv, jobject thisObj);

private:
  VehiclePosition getVehiclePosition(const CvPoint2D64f centroid);

  void saveConfig();
  void loadConfig();
};
