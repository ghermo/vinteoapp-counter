#include <BlobTracking_jni.h>
#include <package_tracking/BlobTracking.h>

#include <android/log.h>

#define LOG_TAG "Vinteo/bgs"
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))

using namespace std;
using namespace cv;
using namespace cvb;


BlobTracking::BlobTracking() : firstTime(true), minArea(500), maxArea(20000), debugTrack(false), debugBlob(false), showBlobMask(false), showOutput(true)
{
  std::cout << "BlobTracking()" << std::endl;
}

BlobTracking::~BlobTracking()
{
  std::cout << "~BlobTracking()" << std::endl;
}

const cvb::CvTracks BlobTracking::getTracks()
{
  return tracks;
}

void BlobTracking::process(const cv::Mat &img_input, const cv::Mat &img_mask, cv::Mat &img_output)
{
  if(img_input.empty() || img_mask.empty())
    return;

  loadConfig();

  if(firstTime)
    saveConfig();

  IplImage* frame = new IplImage(img_input);
  cvConvertScale(frame, frame, 1, 0);

  IplImage* segmentated = new IplImage(img_mask);
  
  IplConvKernel* morphKernel = cvCreateStructuringElementEx(5, 5, 1, 1, CV_SHAPE_RECT, NULL);
  cvMorphologyEx(segmentated, segmentated, NULL, morphKernel, CV_MOP_OPEN, 1);

  if(showBlobMask)
    cvShowImage("Blob Mask", segmentated);

  IplImage* labelImg = cvCreateImage(cvGetSize(frame), IPL_DEPTH_LABEL, 1);

  cvb::CvBlobs blobs;
  unsigned int result = cvb::cvLabel(segmentated, labelImg, blobs);
  
  //cvb::cvFilterByArea(blobs, 500, 1000000);
  cvb::cvFilterByArea(blobs, minArea, maxArea);

  IplImage* imgOut = cvCreateImage(cvGetSize(frame), IPL_DEPTH_8U, 3);
  
  //cvb::cvRenderBlobs(labelImg, blobs, frame, frame, CV_BLOB_RENDER_BOUNDING_BOX);
  if(debugBlob)
    cvb::cvRenderBlobs(labelImg, blobs, frame, imgOut, CV_BLOB_RENDER_BOUNDING_BOX|CV_BLOB_RENDER_CENTROID|CV_BLOB_RENDER_ANGLE|CV_BLOB_RENDER_TO_STD);
  else
    cvb::cvRenderBlobs(labelImg, blobs, frame, imgOut, CV_BLOB_RENDER_BOUNDING_BOX|CV_BLOB_RENDER_CENTROID|CV_BLOB_RENDER_ANGLE);

  cvb::cvUpdateTracks(blobs, tracks, 200., 5);
  
  if(debugTrack)
    cvb::cvRenderTracks(tracks, imgOut, imgOut, CV_TRACK_RENDER_ID|CV_TRACK_RENDER_BOUNDING_BOX|CV_TRACK_RENDER_TO_STD);
  else
    cvb::cvRenderTracks(tracks, imgOut, imgOut, CV_TRACK_RENDER_ID|CV_TRACK_RENDER_BOUNDING_BOX);
  
  //std::map<CvID, CvTrack *> CvTracks

//  if(showOutput)
//    cvShowImage("Blob Tracking", frame);

  cv::Mat img_result(imgOut);
  img_result.copyTo(img_output);

  //cvReleaseImage(&frame);
  //cvReleaseImage(&segmentated);
  cvReleaseImage(&labelImg);
  cvReleaseImage(&imgOut);
  delete frame;
  delete segmentated;
  cvReleaseBlobs(blobs);
  cvReleaseStructuringElement(&morphKernel);

  firstTime = false;
}

void BlobTracking::saveConfig()
{
//  CvFileStorage* fs = cvOpenFileStorage("config/BlobTracking.xml", 0, CV_STORAGE_WRITE);
//
//  cvWriteInt(fs, "minArea", minArea);
//  cvWriteInt(fs, "maxArea", maxArea);
//
//  cvWriteInt(fs, "debugTrack", debugTrack);
//  cvWriteInt(fs, "debugBlob", debugBlob);
//  cvWriteInt(fs, "showBlobMask", showBlobMask);
//  cvWriteInt(fs, "showOutput", showOutput);
//
//  cvReleaseFileStorage(&fs);
}

void BlobTracking::loadConfig()
{
//  CvFileStorage* fs = cvOpenFileStorage("config/BlobTracking.xml", 0, CV_STORAGE_READ);
//
//  minArea = cvReadIntByName(fs, 0, "minArea", 500);
//  maxArea = cvReadIntByName(fs, 0, "maxArea", 20000);
//
//  debugTrack = cvReadIntByName(fs, 0, "debugTrack", false);
//  debugBlob = cvReadIntByName(fs, 0, "debugBlob", false);
//  showBlobMask = cvReadIntByName(fs, 0, "showBlobMask", false);
//  showOutput = cvReadIntByName(fs, 0, "showOutput", true);
//
//  cvReleaseFileStorage(&fs);
//
  minArea = 500;
  maxArea = 20000;

  debugTrack = false;
  debugBlob = false;
  showBlobMask = false;
  showOutput = true;

}

JNIEXPORT jlong JNICALL Java_com_vinteoinc_ghermo_vinteopeoplecounter_analysis_BlobTracking_nativeCreateObject
(JNIEnv * jenv, jclass)
{
    //LOGD("Java_com_vinteoinc_ghermo_vinteopeoplecounter_DetectionBasedTracker_nativeCreateObject enter");
    jlong result = 0;

    try
    {
        result = (jlong)new BlobTracking;
    }
    catch(cv::Exception& e)
    {
        //LOGD("nativeCreateObject caught cv::Exception: %s", e.what());
        jclass je = jenv->FindClass("org/opencv/core/CvException");
        if(!je)
            je = jenv->FindClass("java/lang/Exception");
        jenv->ThrowNew(je, e.what());
    }
    catch (...)
    {
        //LOGD("nativeCreateObject caught unknown exception");
        jclass je = jenv->FindClass("java/lang/Exception");
        jenv->ThrowNew(je, "Unknown exception in JNI code of DetectionBasedTracker.nativeCreateObject()");
        return 0;
    }

    //LOGD("Java_com_vinteoinc_ghermo_vinteopeoplecounter_DetectionBasedTracker_nativeCreateObject exit");
    return result;
}

JNIEXPORT void JNICALL Java_com_vinteoinc_ghermo_vinteopeoplecounter_analysis_BlobTracking_nativeDestroyObject
(JNIEnv * jenv, jclass, jlong thiz)
{
    //LOGD("Java_com_vinteoinc_ghermo_vinteopeoplecounter_DetectionBasedTracker_nativeDestroyObject enter");
    try
    {
        if(thiz != 0)
        {
            delete (BlobTracking*)thiz;
        }
    }
    catch(cv::Exception& e)
    {
        //LOGD("nativeestroyObject caught cv::Exception: %s", e.what());
        jclass je = jenv->FindClass("org/opencv/core/CvException");
        if(!je)
            je = jenv->FindClass("java/lang/Exception");
        jenv->ThrowNew(je, e.what());
    }
    catch (...)
    {
        //LOGD("nativeDestroyObject caught unknown exception");
        jclass je = jenv->FindClass("java/lang/Exception");
        jenv->ThrowNew(je, "Unknown exception in JNI code of DetectionBasedTracker.nativeDestroyObject()");
    }
    //LOGD("Java_com_vinteoinc_ghermo_vinteopeoplecounter_DetectionBasedTracker_nativeDestroyObject exit");
}

JNIEXPORT void JNICALL Java_com_vinteoinc_ghermo_vinteopeoplecounter_analysis_BlobTracking_nativeProcess
  (JNIEnv * jenv, jclass, jlong thiz, jlong img_input, jlong img_mask, jlong img_output) {

  //LOGD("GRG 2 Native Process Function enter");
      try
          {
              ((BlobTracking*)thiz)->process(*((Mat*)img_input), *((Mat*)img_mask), *((Mat*)img_output));
          }
          catch(cv::Exception& e)
          {
              //LOGD("nativeCreateObject caught cv::Exception: %s", e.what());
              jclass je = jenv->FindClass("org/opencv/core/CvException");
              if(!je)
                  je = jenv->FindClass("java/lang/Exception");
              jenv->ThrowNew(je, e.what());
          }
          catch (...)
          {
              //LOGD("nativeDetect caught unknown exception");
              jclass je = jenv->FindClass("java/lang/Exception");
              jenv->ThrowNew(je, "Unknown exception in JNI code DetectionBasedTracker.nativeDetect()");
          }

      //LOGD("Native Process Function exit");

  }

JNIEXPORT jlong JNICALL Java_com_vinteoinc_ghermo_vinteopeoplecounter_analysis_BlobTracking_nativeGetTracks
  (JNIEnv * jenv, jclass, jlong thiz)
  {

    jlong jresult = 0 ;
    cvb::CvTracks result;

    result = ((BlobTracking*)thiz)->getTracks();
    *(cvb::CvTracks **)&jresult = new cvb::CvTracks((const cvb::CvTracks &)result);
    return jresult;
  }