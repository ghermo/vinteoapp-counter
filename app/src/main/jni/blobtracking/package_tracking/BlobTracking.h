#pragma once

#include <iostream>
#include <cv.h>
#include <highgui.h>

#include "cvblob/cvblob.h"

#include "cvblob/cvblob.h"
#include "cvblob/cvtrack.cpp"
#include "cvblob/cvlabel.cpp"
#include "cvblob/cvblob.cpp"

class BlobTracking
{
private:
  bool firstTime;
  int minArea;
  int maxArea;
  
  bool debugTrack;
  bool debugBlob;
  bool showBlobMask;
  bool showOutput;

  cvb::CvTracks tracks;
  void saveConfig();
  void loadConfig();

public:
  BlobTracking();
  ~BlobTracking();

  void process(const cv::Mat &img_input, const cv::Mat &img_mask, cv::Mat &img_output);
  const cvb::CvTracks getTracks();
};

