LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

#OPENCV_CAMERA_MODULES:=off
#OPENCV_INSTALL_MODULES:=off
#OPENCV_LIB_TYPE:=SHARED
include /home/grg021/dev/opencv/OpenCV-2.4.10-android-sdk/sdk/native/jni/OpenCV.mk

LOCAL_SRC_FILES  := blobtracking/BlobTracking_jni.cpp
LOCAL_C_INCLUDES += $(LOCAL_PATH)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/blobtracking
LOCAL_LDLIBS     += -llog -ldl
LOCAL_MODULE     := blobtracking

include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)

#OPENCV_CAMERA_MODULES:=off
#OPENCV_INSTALL_MODULES:=off
#OPENCV_LIB_TYPE:=SHARED
include /home/grg021/dev/opencv/OpenCV-2.4.10-android-sdk/sdk/native/jni/OpenCV.mk

LOCAL_SRC_FILES  := analysis/PeopleCounting.cpp
LOCAL_C_INCLUDES += $(LOCAL_PATH)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/analysis
LOCAL_LDLIBS     += -llog -ldl
LOCAL_MODULE     := peoplecounting

include $(BUILD_SHARED_LIBRARY)