# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/grg021/dev/android-studio/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-dontskipnonpubliclibraryclasses
-dontobfuscate
-forceprocessing
-optimizationpasses 5

-keep class org.opencv.** { *; }
-keep class com.vinteoinc.ghermo.vinteopeoplecounter.MainActivity { *; }
-keep class com.vinteoinc.ghermo.vinteopeoplecounter.analysis.BlobTracking { *; }
-keep class com.vinteoinc.ghermo.vinteopeoplecounter.analysis.PeopleCounting { *; }
-keep class * extends android.app.Activity
-assumenosideeffects class android.util.Log {
    public static *** d(...);
    public static *** v(...);
}